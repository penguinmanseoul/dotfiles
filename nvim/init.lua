require 'penguin.lazy'
require 'penguin.options'
require 'penguin.keymaps'
require 'penguin.tree-sitter'
require 'penguin.comment'
require 'penguin.lualine'
require 'penguin.bufferline'
require 'penguin.telescope'
require 'penguin.which-key'
require 'penguin.nvim-tree'
require 'penguin.lspconfig'

-- require 'kyeongsoo.lsp'
-- require 'kyeongsoo.luasnip'
-- require 'kyeongsoo.cmp'
-- require 'kyeongsoo.cmd'


-- require 'penguin.nvim-tree'
