(setq user-emacs-directory "C:/Users/mando/AppData/Roaming/.emacs.d/")
(setq user-init-file "C:/Users/mando/AppData/Roaming/.emacs.d/init.el")
(setq early-init-file "C:/Users/mando/AppData/Roaming/.emacs.d/early-init.el")
(setq default-directory "C:/Users/mando/")
(setenv "HOME" "C:/Users/mando/")

(add-to-list 'load-path "C:/Users/mando/AppData/Roaming/.emacs.d/scripts/")

(require 'elpaca-setup)

(setq backup-directory-alist '((".*" . "~/Trash/files")))

(use-package evil
             :init
             (setq evil-want-integration t
                   evil-want-keybinding nil
                   evil-vsplit-window-right t
                   evil-split-window-below t
                   evil-undo-system 'undo-redo)
             (evil-mode))

(use-package evil-collection
             :after evil
             :config
             (add-to-list 'evil-collection-mode-list 'help)
             (evil-collection-init))

(use-package evil-tutor)

(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil))

(setq org-return-follows-link t)

;; TODO: 마저할게요
(use-package general
  :config
  (general-evil-setup)
  (general-create-definer penguin/leader-keys
			  :states '(normal insert visual emacs)
			  :keymaps 'override
			  :prefix "SPC"
			  :global-prefix "M-SPC")
  (penguin/leader-keys
    "SPC" '(counsel-M-x :wk "Counsel-M-x")
    "." '(find-file :wk "Find files")
    "=" '(perspective-map :wk "Perspective")
    "TAB TAB" '(comment-line :wk "Comment line")
    "u" '(universal-argument :wk "Universal argument"))

  (penguin/leader-keys
   "b" '(:ignore t :wk "Bookmarks/Buffers")
   "b b" '(switch-to-buffer :wk "Create indirect buffer copy in a split"))

  (penguin/leader-keys
    "h" '(:ignore t :wk "Help")
    "h r r" '((lambda () (interactive)
		(load-file "C:/Users/mando/AppData/Roaming/.emacs.d/init.el")
		(ignore (elpaca-process-queues)))
	      :wk "Reload emacs config"))

  )




(use-package counsel
             :after ivy
             :diminish
             :config
             (counsel-mode)
             (setq ivy-initial-inputs-alist nil))

(use-package ivy
             :bind
             (("C-c C-r" . ivy-resume)
              ("C-x B" . ivy-switch-buffer-other-window))
             :diminish
             :custom
             (setq ivy-use-virtual-buffers t)
             (setq ivy-count-format "(%d/%d) ")
             (setq enable-recursive-minibuffers t)
             :config
             (ivy-mode))

(cua-mode t)
(delete-selection-mode t)
(electric-indent-mode nil)
(electric-pair-mode t)
(global-auto-revert-mode t)
(global-display-line-numbers-mode t)
(global-visual-line-mode t)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(set-face-attribute 'default nil :height 110)
(add-hook 'window-setup-hook 'toggle-frame-maximized t)
(setq inhibit-splash-screen t)
(setq org-edit-src-content-indentation 0)

